import React from 'react';
import { Container, Header } from 'semantic-ui-react';

interface Props {
  title: string;
}

const Banner = ({ title }: Props) => {
  return (
    <Container fluid style={{ backgroundColor: '#00B5AD', padding: '50px 0' }}>
      <Header as="h1" textAlign="center" style={{ color: 'white' }}>
        {title}
      </Header>
    </Container>
  );
};

export default Banner;
