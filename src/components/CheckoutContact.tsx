import React, { ChangeEvent, FormEvent, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import {
  Button,
  Container,
  Form,
  Grid,
  Message,
  Segment,
} from 'semantic-ui-react';
import Banner from './Banner';
import OrderSummary from './OrderSummary';

export default function CheckoutContact() {
  const location: any = useLocation();
  const [userEmail, setUserEmail] = useState('');
  const [firstName, setFirsName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phone, setPhone] = useState('');
  const [error, setError] = useState('');
  const subtotal = location.state.subtotal;
  let history = useHistory();

  function handleEmailChange(e: ChangeEvent<HTMLInputElement>) {
    setUserEmail(e.target.value);
    setError('');
  }

  function handleFirstNameChange(e: ChangeEvent<HTMLInputElement>) {
    setFirsName(e.target.value);
    setError('');
  }

  function handleLastNameChange(e: ChangeEvent<HTMLInputElement>) {
    setLastName(e.target.value);
    setError('');
  }

  function handlePhoneChange(e: ChangeEvent<HTMLInputElement>) {
    setPhone(e.target.value);
    setError('');
  }

  function handleContinueToShipping(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!userEmail) {
      return setError('Email is required!');
    } else if (!firstName) {
      return setError('First name is required!');
    } else if (!lastName) {
      return setError('Last name is required!');
    }

    // Do something with the data

    history.push({
      pathname: '/checkoutShipping',
      state: {
        subtotal: subtotal,
        total: subtotal,
      },
    });
  }

  return (
    <>
      <Banner title="Contact Information" />
      <Container>
        <Grid columns={2} stackable style={{ marginTop: '3em' }}>
          <Grid.Column width={10}>
            <Segment>
              <Form onSubmit={handleContinueToShipping}>
                <Form.Field>
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    onChange={handleEmailChange}
                  />
                </Form.Field>
                <Form.Group widths="equal">
                  <Form.Field>
                    <label>First Name</label>
                    <input
                      type="text"
                      name="firstName"
                      onChange={handleFirstNameChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label>Last Name</label>
                    <input
                      type="text"
                      name="LastName"
                      onChange={handleLastNameChange}
                    />
                  </Form.Field>
                </Form.Group>
                <Form.Field>
                  <label>Phone (Optional)</label>
                  <input
                    type="text"
                    name="phone"
                    onChange={handlePhoneChange}
                  />
                </Form.Field>
                {error && <Message negative>{error}</Message>}
                <Button positive basic>
                  Continue to shipping
                </Button>
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column width={6}>
            <OrderSummary
              subtotal={subtotal}
              tax={0}
              shipping={0}
              total={subtotal}
            />
          </Grid.Column>
        </Grid>
      </Container>
    </>
  );
}
