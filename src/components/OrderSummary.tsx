import React from 'react';
import { Divider, Grid, Segment } from 'semantic-ui-react';

interface Props {
  subtotal: number;
  tax: number;
  shipping: number;
  total: number;
}

export default function OrderSummary({
  subtotal,
  tax,
  shipping,
  total,
}: Props) {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 2,
  });

  return (
    <Segment style={{ backgroundColor: '#f9f9f9' }}>
      <Grid columns={2}>
        <Grid.Column>Subtotal</Grid.Column>
        <Grid.Column style={{ textAlign: 'right' }}>
          {formatter.format(subtotal)}
        </Grid.Column>
      </Grid>
      <Grid columns={2}>
        <Grid.Column>Tax</Grid.Column>
        <Grid.Column style={{ textAlign: 'right' }}>
          {formatter.format(Math.round(100 * tax) / 100)}
        </Grid.Column>
      </Grid>
      <Grid columns={2}>
        <Grid.Column>Shipping</Grid.Column>
        <Grid.Column style={{ textAlign: 'right' }}>
          {formatter.format(Math.round(100 * shipping) / 100)}
        </Grid.Column>
      </Grid>
      <Grid.Row>
        <Divider section />
      </Grid.Row>
      <Grid columns={2}>
        <Grid.Column>Total</Grid.Column>
        <Grid.Column style={{ textAlign: 'right' }}>
          {formatter.format(Math.round(100 * total) / 100)}
        </Grid.Column>
      </Grid>
    </Segment>
  );
}
