import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Segment,
} from 'semantic-ui-react';
import { item } from '../models/item';
import { user } from '../models/user';
import Banner from './Banner';
import CartItem from './CartItem';

interface CartList {
  [id: string]: item;
}

interface Props {
  cart: CartList;
  removeFromCart: (cartItemId: string) => void;
  clearCart: () => void;
  checkout: () => void;
  user: user | null;
}

export default function Cart({
  cart,
  removeFromCart,
  clearCart,
  checkout,
  user,
}: Props) {
  const cartKeys = Object.keys(cart || {});
  const [subtotal, setSubtotal] = useState<number>(0);

  let history = useHistory();

  useEffect(() => {
    getSubtotal();
  }, [cart, subtotal]);

  function getSubtotal() {
    let sub = 0;

    cartKeys.map((key) => {
      sub = sub + cart[key].amount * cart[key].product.price;
    });

    setSubtotal(sub);
  }

  return (
    <>
      <Banner title="My Cart" />
      {cartKeys.length ? (
        <Container>
          <Grid columns={2} stackable style={{ marginTop: '3em' }}>
            <Grid.Column width={12}>
              <Grid columns={2}>
                {cartKeys.map((key) => (
                  <Grid.Column width={8} key={key}>
                    <CartItem
                      cartKey={key}
                      cartItem={cart[key]}
                      removeFromCart={removeFromCart}
                    />
                  </Grid.Column>
                ))}
              </Grid>
            </Grid.Column>
            <Grid.Column width={4}>
              <Segment>
                <h2>SUBTOTAL</h2>
                <p>${subtotal}</p>
                <Button
                  fluid
                  positive
                  basic
                  onClick={() => {
                    if (!user) {
                      history.push('/login');
                    } else {
                      //checkout();
                      history.push({
                        pathname: '/checkoutContact',
                        state: {
                          subtotal: subtotal,
                          total: subtotal,
                        },
                      });
                    }
                  }}
                >
                  Checkout
                </Button>
                <Divider section />
                <div style={{ textAlign: 'center' }}>
                  <Link to="/products" style={{ color: 'black' }}>
                    Continue shopping
                  </Link>
                </div>
              </Segment>
            </Grid.Column>
          </Grid>
        </Container>
      ) : (
        <Header textAlign="center" as="h2" style={{ marginTop: '3em' }}>
          There are no products in your cart!
        </Header>
      )}
    </>
  );
}
