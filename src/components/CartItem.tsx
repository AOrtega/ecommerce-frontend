import React from 'react';
import { Button, Card, Image, Label } from 'semantic-ui-react';
import { item } from '../models/item';

interface Props {
  cartKey: string;
  cartItem: item;
  removeFromCart: (cartItemId: string) => void;
}

export default function CartItem({ cartKey, cartItem, removeFromCart }: Props) {
  const { product, amount } = cartItem;

  return (
    <Card fluid>
      <Image src="https://i.stack.imgur.com/y9DpT.jpg" wrapped ui={false} />
      <Card.Content>
        <Card.Header>
          {product.name}{' '}
          <span style={{ marginLeft: '0.25em' }}>
            <Label color="teal">{`$${product.price}`}</Label>
          </span>
        </Card.Header>
        <Card.Description>
          <div>{product.shortDesc}</div>
          <small>{`${amount} in cart`}</small>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button negative basic onClick={() => removeFromCart(cartKey)}>
          Remove from cart
        </Button>
      </Card.Content>
    </Card>
  );
}
