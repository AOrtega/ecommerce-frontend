import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import {
  Button,
  Container,
  Form,
  Grid,
  Message,
  Segment,
} from 'semantic-ui-react';
import Banner from './Banner';
import OrderSummary from './OrderSummary';

const country = [{ key: 'usa', text: 'United States', value: 'usa' }];
const state = [
  { key: 'al', text: 'Alabama', value: 'al' },
  { key: 'ak', text: 'Alaska', value: 'ak' },
  { key: 'az', text: 'Arizona', value: 'az' },
  { key: 'ar', text: 'Arkansas', value: 'ar' },
  { key: 'ca', text: 'California', value: 'ca' },
  { key: 'co', text: 'Colorado', value: 'co' },
  { key: 'ct', text: 'Connecticut', value: 'ct' },
  { key: 'dc', text: 'Washington DC', value: 'dc' },
  { key: 'de', text: 'Delaware', value: 'de' },
  { key: 'fl', text: 'Florida', value: 'fl' },
  { key: 'ga', text: 'Georgia', value: 'ga' },
  { key: 'gu', text: 'Guam', value: 'gu' },
  { key: 'hi', text: 'Hawaii', value: 'hi' },
  { key: 'id', text: 'Idaho', value: 'id' },
  { key: 'il', text: 'Illinois', value: 'il' },
  { key: 'in', text: 'Indiana', value: 'in' },
  { key: 'ia', text: 'Iowa', value: 'ia' },
  { key: 'ks', text: 'Kansas', value: 'hs' },
  { key: 'ky', text: 'kentucky', value: 'ky' },
  { key: 'la', text: 'Louisiana', value: 'la' },
  { key: 'me', text: 'Maine', value: 'me' },
  { key: 'md', text: 'Maryland', value: 'md' },
  { key: 'ma', text: 'Massachusetts', value: 'ma' },
  { key: 'mi', text: 'Michigan', value: 'mi' },
  { key: 'mn', text: 'Minnesota', value: 'mn' },
  { key: 'ms', text: 'Missisippi', value: 'ms' },
  { key: 'mo', text: 'Misouri', value: 'mo' },
  { key: 'mt', text: 'Montana', value: 'mt' },
  { key: 'ne', text: 'Nebraska', value: 'ne' },
  { key: 'nv', text: 'Nevada', value: 'nv' },
  { key: 'nh', text: 'New Hampshire', value: 'nh' },
  { key: 'nj', text: 'New Jersey', value: 'nj' },
  { key: 'nm', text: 'New Mexico', value: 'nm' },
  { key: 'ny', text: 'New York', value: 'ny' },
  { key: 'nc', text: 'North Carolina', value: 'nc' },
  { key: 'nd', text: 'North Dakota', value: 'nd' },
  { key: 'oh', text: 'Ohio', value: 'oh' },
  { key: 'ok', text: 'Oklahoma', value: 'ok' },
  { key: 'or', text: 'Oregon', value: 'or' },
  { key: 'pa', text: 'Pennsylvania', value: 'pa' },
  { key: 'ri', text: 'Rhode Island', value: 'ri' },
  { key: 'sc', text: 'South Carolina', value: 'sc' },
  { key: 'sd', text: 'South Dakota', value: 'sd' },
  { key: 'tn', text: 'Tennessee', value: 'tn' },
  { key: 'tx', text: 'Texas', value: 'tx' },
  { key: 'ut', text: 'Utah', value: 'ut' },
  { key: 'vt', text: 'Vermont', value: 'vt' },
  { key: 'va', text: 'Virginia', value: 'va' },
  { key: 'wa', text: 'Washington', value: 'wa' },
  { key: 'wv', text: 'West Virginia', value: 'wv' },
  { key: 'wi', text: 'Wisconsin', value: 'wi' },
  { key: 'wy', text: 'Wyoming', value: 'wu' },
];

export default function CheckoutShipping() {
  const location: any = useLocation();
  const subtotal = location.state.subtotal;
  const [tax, setTax] = useState<number>(0);
  const [shipping, setShipping] = useState<number>(0);
  const [total, setTotal] = useState<number>(0);
  const [company, setCompany] = useState('');
  const [address1, setAddress1] = useState('');
  const [address2, setAddress2] = useState('');
  const [city, setCity] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [error, setError] = useState('');
  let history = useHistory();

  useEffect(() => {
    getTax();
    getShipping();
    setTotal(subtotal + tax + shipping);
  }, [tax, shipping, total]);

  function getTax() {
    // 5.6 is the hardcoded az sales tax rate
    setTax(subtotal * (5.6 / 100));
  }

  function getShipping() {
    // Shipping rate is hardcoded
    setShipping(12.8);
  }

  function handleCompanyChange(e: ChangeEvent<HTMLInputElement>) {
    setCompany(e.target.value);
    setError('');
  }

  function handleAddress1Change(e: ChangeEvent<HTMLInputElement>) {
    setAddress1(e.target.value);
    setError('');
  }

  function handleAddress2Change(e: ChangeEvent<HTMLInputElement>) {
    setAddress2(e.target.value);
    setError('');
  }

  function handleCityChange(e: ChangeEvent<HTMLInputElement>) {
    setCity(e.target.value);
    setError('');
  }

  function handleZipCodeChange(e: ChangeEvent<HTMLInputElement>) {
    setZipCode(e.target.value);
    setError('');
  }

  function handleContinueToShipping(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!address1) {
      return setError('Address is required!');
    } else if (!city) {
      return setError('City is required!');
    } else if (!zipCode) {
      return setError('Zip code is required!');
    }

    // Do something with the data

    history.push({
      pathname: '/checkoutPayment',
      state: {
        subtotal: subtotal,
        tax: tax,
        shipping: shipping,
        total: total,
      },
    });
  }

  return (
    <>
      <Banner title="Shipping Information" />
      <Container>
        <Grid columns={2} stackable style={{ marginTop: '3em' }}>
          <Grid.Column width={10}>
            <Segment>
              <Form onSubmit={handleContinueToShipping}>
                <Form.Field>
                  <label>Company (Optional)</label>
                  <input
                    type="text"
                    name="company"
                    onChange={handleCompanyChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Address</label>
                  <input
                    type="text"
                    name="address1"
                    onChange={handleAddress1Change}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Address 2 (Optional)</label>
                  <input
                    type="text"
                    name="address2"
                    onChange={handleAddress2Change}
                  />
                </Form.Field>
                <Form.Field>
                  <label>City</label>
                  <input type="text" name="city" onChange={handleCityChange} />
                </Form.Field>
                <Form.Group widths="equal">
                  <Form.Field>
                    <Form.Select fluid label="Country:" options={country} />
                  </Form.Field>
                  <Form.Field>
                    <Form.Select fluid label="State:" options={state} />
                  </Form.Field>
                  <Form.Field>
                    <label>Zip Code</label>
                    <input
                      type="text"
                      name="zipCode"
                      onChange={handleZipCodeChange}
                    />
                  </Form.Field>
                </Form.Group>
                {error && <Message negative>{error}</Message>}
                <Button positive basic>
                  Continue to payment
                </Button>
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column width={6}>
            <OrderSummary
              subtotal={subtotal}
              tax={tax}
              shipping={shipping}
              total={total}
            />
          </Grid.Column>
        </Grid>
      </Container>
    </>
  );
}
