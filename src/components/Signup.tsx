import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Container, Form, Message, Segment } from 'semantic-ui-react';
import { user } from '../models/user';
import Banner from './Banner';

interface Props {
  user: user | null;
  handleSignup: (username: string, password: string) => Promise<boolean>;
}

export default function Signup({ user, handleSignup }: Props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [error, setError] = useState('');

  function handleEmailChange(e: ChangeEvent<HTMLInputElement>) {
    setUsername(e.target.value);
    setError('');
  }

  function handlePasswordChange(e: ChangeEvent<HTMLInputElement>) {
    setPassword(e.target.value);
    setError('');
  }

  function handleConfirmPasswordChange(e: ChangeEvent<HTMLInputElement>) {
    setConfirmPassword(e.target.value);
    setError('');
  }

  function signup(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!username || !password || !confirmPassword) {
      return setError('Email and Password are required!');
    }

    if (password !== confirmPassword) {
      return setError('Passwords do not match!');
    }

    handleSignup(username, password).then((loggedin) => {
      if (!loggedin) {
        setError('Email already in use');
      }
    });
  }

  return !user ? (
    <>
      <Banner title="Signup" />
      <Container>
        <Segment style={{ marginTop: '3em' }}>
          <Form onSubmit={signup}>
            <Form.Field>
              <label>Email</label>
              <input
                type="email"
                name="username"
                onChange={handleEmailChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input
                type="password"
                name="password"
                onChange={handlePasswordChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Confirm Password</label>
              <input
                type="password"
                name="password"
                onChange={handleConfirmPasswordChange}
              />
            </Form.Field>
            {error && <Message negative>{error}</Message>}
            <Button basic color="teal" type="submit">
              Signup
            </Button>
          </Form>
        </Segment>
      </Container>
    </>
  ) : (
    <Redirect to="/products" />
  );
}
