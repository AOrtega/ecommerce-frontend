import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';

import Banner from './Banner';

import { user } from '../models/user';
import { Button, Container, Form, Message, Segment } from 'semantic-ui-react';

interface Props {
  user: user | null;
  handleLogin: (username: string, password: string) => Promise<boolean>;
}

export default function Login({ user, handleLogin }: Props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  function handleEmailChange(e: ChangeEvent<HTMLInputElement>) {
    setUsername(e.target.value);
    setError('');
  }

  function handlePasswordChange(e: ChangeEvent<HTMLInputElement>) {
    setPassword(e.target.value);
    setError('');
  }

  function login(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!username || !password) {
      return setError('Email and Password are required!');
    }

    handleLogin(username, password).then((loggedin) => {
      if (!loggedin) {
        setError('Wrong Email or Password');
      }
    });
  }

  return !user ? (
    <>
      <Banner title="Login" />
      <Container>
        <Segment style={{ marginTop: '3em' }}>
          <Form onSubmit={login}>
            <Form.Field>
              <label>Email</label>
              <input
                type="email"
                name="username"
                onChange={handleEmailChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input
                type="password"
                name="password"
                onChange={handlePasswordChange}
              />
            </Form.Field>
            {error && <Message negative>{error}</Message>}
            <Button basic color="teal" type="submit">
              Login
            </Button>
          </Form>
        </Segment>
        <Link to="/signup">Don't have an account?</Link>
      </Container>
    </>
  ) : (
    <Redirect to="/products" />
  );
}
