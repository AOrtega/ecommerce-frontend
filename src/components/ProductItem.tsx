import React from 'react';
import { Button, Card, Image, Label } from 'semantic-ui-react';
import { item } from '../models/item';

import { product } from '../models/product';

interface Props {
  product: product;
  addToCart: (cartItem: item) => void;
}

export default function ProductItem({ product, addToCart }: Props) {
  return (
    <Card fluid>
      <Image src="https://i.stack.imgur.com/y9DpT.jpg" wrapped ui={false} />
      <Card.Content>
        <Card.Header>
          {product.name}{' '}
          <span style={{ marginLeft: '0.25em' }}>
            <Label color="teal">{`$${product.price}`}</Label>
          </span>
        </Card.Header>
        {product.stock > 0 ? (
          <Card.Meta>{`${product.stock} Available`}</Card.Meta>
        ) : (
          <Card.Meta>Out Of Stock!</Card.Meta>
        )}
        <Card.Description>{product.shortDesc}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button
          basic
          color="teal"
          disabled={product.stock > 0 ? false : true}
          onClick={() =>
            addToCart({
              id: product.name,
              product,
              amount: 1,
            })
          }
        >
          Add to cart
        </Button>
      </Card.Content>
    </Card>
  );
}
