import React, { MouseEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Label, Menu } from 'semantic-ui-react';
import { item } from '../models/item';
import { user } from '../models/user';

interface CartList {
  [id: string]: item;
}

interface Props {
  user: user | null;
  cart: CartList;
  logout: (e: MouseEvent<HTMLAnchorElement>) => void;
}

export default function NavBar({ user, cart, logout }: Props) {
  let history = useHistory();

  return (
    <Container>
      <Menu secondary>
        <Menu.Item>
          <strong>E-Commerce</strong>
        </Menu.Item>
        <Menu.Item name="products" onClick={() => history.push('/products')} />
        {user && user.accessLevel < 1 && (
          <Menu.Item
            name="add product"
            onClick={() => history.push('/add-product')}
          />
        )}
        <Menu.Item name="cart" onClick={() => history.push('/cart')}>
          Cart
          <span style={{ marginLeft: '1em' }}>
            <Label color="teal">{Object.keys(cart).length}</Label>
          </span>
        </Menu.Item>
        <Menu.Menu position="right">
          {!user ? (
            <Menu.Item name="login" onClick={() => history.push('/login')} />
          ) : (
            <Menu.Item name="logout" onClick={logout} />
          )}
        </Menu.Menu>
      </Menu>
    </Container>
  );
}
