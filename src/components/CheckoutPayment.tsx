import React, { ChangeEvent, FormEvent, useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  Button,
  Container,
  Form,
  Grid,
  Message,
  Segment,
} from 'semantic-ui-react';
import Banner from './Banner';
import OrderSummary from './OrderSummary';

export default function CheckoutPayment() {
  const location: any = useLocation();
  const subtotal = location.state.subtotal;
  const tax = location.state.tax;
  const shipping = location.state.shipping;
  const total = location.state.total;
  const [cardNumber, setCardNumber] = useState('');
  const [nameOnCard, setNameOnCard] = useState('');
  const [expDate, setExpDate] = useState('');
  const [cvv, setCvv] = useState('');
  const [error, setError] = useState('');

  function handleCardChange(e: ChangeEvent<HTMLInputElement>) {
    setCardNumber(e.target.value);
    setError('');
  }

  function handleNameOnCardChange(e: ChangeEvent<HTMLInputElement>) {
    setNameOnCard(e.target.value);
    setError('');
  }

  function handleExpirationDateChange(e: ChangeEvent<HTMLInputElement>) {
    setExpDate(e.target.value);
    setError('');
  }

  function handleSecurityCodeChange(e: ChangeEvent<HTMLInputElement>) {
    setCvv(e.target.value);
    setError('');
  }

  function pay(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!cardNumber) {
      return setError('Card number is required!');
    } else if (!nameOnCard) {
      return setError('Name on card is required!');
    } else if (!expDate) {
      return setError('Expiration date is required!');
    } else if (!cvv) {
      return setError('Security code is required!');
    }

    // Do something with the data and process payment
  }

  return (
    <>
      <Banner title="Payment Information" />
      <Container>
        <Grid columns={2} stackable style={{ marginTop: '3em' }}>
          <Grid.Column width={10}>
            <Segment>
              <Form onSubmit={pay}>
                <Form.Field>
                  <label>Card Number</label>
                  <input
                    type="text"
                    name="cardNumber"
                    onChange={handleCardChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Name on card</label>
                  <input
                    type="text"
                    name="nameOnCard"
                    onChange={handleNameOnCardChange}
                  />
                </Form.Field>
                <Form.Group widths="equal">
                  <Form.Field>
                    <label>Expiration date</label>
                    <input
                      type="text"
                      name="expDate"
                      onChange={handleExpirationDateChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label>Security code</label>
                    <input
                      type="text"
                      name="cvv"
                      onChange={handleSecurityCodeChange}
                    />
                  </Form.Field>
                </Form.Group>
                {error && <Message negative>{error}</Message>}
                <Button positive basic>
                  Pay now
                </Button>
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column width={6}>
            <OrderSummary
              subtotal={subtotal}
              tax={tax}
              shipping={shipping}
              total={total}
            />
          </Grid.Column>
        </Grid>
      </Container>
    </>
  );
}
