import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import { user } from '../models/user';
import Banner from './Banner';
import { Button, Container, Form, Message, Segment } from 'semantic-ui-react';

interface Props {
  user: user | null;
}

interface Flash {
  status: string;
  msg: string;
}

export default function AddProduct({ user }: Props) {
  const initState = {
    name: '',
    price: 0,
    stock: 0,
    shortDesc: '',
    description: '',
  };
  const [product, setProduct] = useState(initState);
  const [flash, setFlash] = useState<Flash | null>(null);

  async function save(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const { name, price, stock, shortDesc, description } = product;

    if (name && price) {
      const id =
        Math.random().toString(36).substring(2) + Date.now().toString(36);
      const user = JSON.parse(JSON.stringify(localStorage.getItem('user')));
      const { token } = JSON.parse(user);

      await axios.post(
        'http://localhost:3090/products',
        {
          id,
          name,
          price,
          stock,
          shortDesc,
          description,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      );

      setFlash({
        status: 'positive',
        msg: 'Product added successfully',
      });
    } else {
      setFlash({ status: 'negative', msg: 'Please enter a name and price ' });
    }
  }

  function handleChange(
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const { name, value } = e.target;
    setProduct({ ...product, [name]: value });
  }

  return !(user && user.accessLevel < 1) ? (
    <Redirect to="/" />
  ) : (
    <>
      <Banner title="Add Product" />
      <Container>
        <Segment style={{ marginTop: '3em' }}>
          <Form onSubmit={save}>
            <Form.Field>
              <label>Product Name:</label>
              <input
                type="text"
                name="name"
                value={product.name}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Price:</label>
              <input
                type="number"
                name="price"
                value={product.price}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Available in Stock:</label>
              <input
                type="number"
                name="stock"
                value={product.stock}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Short Description:</label>
              <input
                type="text"
                name="shortDesc"
                value={product.shortDesc}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Description</label>
              <textarea
                style={{ resize: 'none' }}
                name="description"
                value={product.description}
                onChange={handleChange}
              />
            </Form.Field>
            {flash && (
              <Message
                positive={flash.status === 'positive' ? true : false}
                negative={flash.status === 'negative' ? true : false}
              >
                {flash.msg}
              </Message>
            )}
            <Button basic color="teal" type="submit">
              Add
            </Button>
          </Form>
        </Segment>
      </Container>
    </>
  );
}
