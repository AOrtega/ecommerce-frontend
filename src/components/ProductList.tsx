import React from 'react';

import { product } from '../models/product';
import ProductItem from './ProductItem';
import Banner from './Banner';
import { item } from '../models/item';
import Grid from 'semantic-ui-react/dist/commonjs/collections/Grid';
import { Container, Header } from 'semantic-ui-react';

interface Props {
  products: product[];
  addToCart: (cartItem: item) => void;
}

export default function ProductList({ products, addToCart }: Props) {
  return (
    <>
      <Banner title="Our Products" />
      {products && products.length ? (
        <Container>
          <Grid columns={3} stackable style={{ marginTop: '3em' }}>
            {products.map((product: product, index: number) => (
              <Grid.Column key={index}>
                <ProductItem product={product} addToCart={addToCart} />
              </Grid.Column>
            ))}
          </Grid>
        </Container>
      ) : (
        <Header textAlign="center" as="h2" style={{ marginTop: '3em' }}>
          No products found!
        </Header>
      )}
    </>
  );
}
