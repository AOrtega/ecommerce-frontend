import React from 'react';
import AppContext from './AppContext';

interface Props {
  WrappedComponent: () => any;
}

const withContext = ({ WrappedComponent }: Props) => {
  const WithHOC = (props: any) => {
    return (
      <AppContext.Consumer>
        {(context) => <WrappedComponent {...props} context={context} />}
      </AppContext.Consumer>
    );
  };

  return WithHOC;
};

export default withContext;
