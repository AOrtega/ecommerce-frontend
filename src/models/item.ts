import { product } from './product';

export interface item {
  id: string;
  product: product;
  amount: number;
}
