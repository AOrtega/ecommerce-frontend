export interface product {
  id: string;
  name: string;
  stock: number;
  price: number;
  shortDesc: string;
  description: string;
}
