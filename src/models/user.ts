export interface user {
  email: string;
  accessLevel: number;
  token: string;
}
