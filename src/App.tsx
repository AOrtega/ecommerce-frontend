import React, { useEffect, useState, MouseEvent } from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';

import { user } from './models/user';
import { product } from './models/product';
import { item } from './models/item';
import AddProduct from './components/AddProduct';
import Cart from './components/Cart';
import Login from './components/Login';
import ProductList from './components/ProductList';
import NavBar from './components/NavBar';
import Signup from './components/Signup';
import CheckoutContact from './components/CheckoutContact';
import CheckoutShipping from './components/CheckoutShipping';
import CheckoutPayment from './components/CheckoutPayment';

interface CartList {
  [id: string]: item;
}

export default function App() {
  const [user, setUser] = useState<user | null>(null);
  const [cart, setCart] = useState<CartList>({});
  const [products, setProducts] = useState<product[]>([]);

  const baseUrl = 'http://localhost:3090';

  useEffect(() => {
    let currentUser = localStorage.getItem('user');
    let currentCart = localStorage.getItem('cart');

    getProduct();
    currentUser = currentUser ? JSON.parse(currentUser) : null;

    if (currentCart === 'undefined' || currentCart === null) {
      localStorage.setItem('cart', JSON.stringify({}));
      setCart({});
    } else {
      setCart(JSON.parse(currentCart!));
    }

    setUser(JSON.parse(JSON.stringify(currentUser)));
  }, []);

  async function login(email: string, password: string): Promise<boolean> {
    const res = await axios
      .post(`${baseUrl}/login`, {
        email,
        password,
      })
      .then((res) => {
        if (res.status === 200) {
          const user: user = {
            email: res.data.email,
            token: res.data.accessToken,
            accessLevel: res.data.role === 'Admin' ? 0 : 1,
          };
          setUser(user);
          localStorage.setItem('user', JSON.stringify(user));
          return true;
        } else {
          return false;
        }
      })
      .catch((res) => {
        return false;
      });

    return res;
  }

  async function signUp(email: string, password: string): Promise<boolean> {
    const res = await axios
      .post(`${baseUrl}/signup`, {
        email,
        password,
      })
      .then((res) => {
        if (res.status === 200) {
          const user: user = {
            email: res.data.email,
            token: res.data.accessToken,
            accessLevel: res.data.role === 'Admin' ? 0 : 1,
          };
          setUser(user);
          localStorage.setItem('user', JSON.stringify(user));
          return true;
        } else {
          return false;
        }
      })
      .catch((res) => {
        return false;
      });

    return res;
  }

  function logout(e: MouseEvent<HTMLAnchorElement>) {
    e.preventDefault();
    setUser(null);
    localStorage.removeItem('user');
  }

  async function getProduct() {
    const productList = await axios.get(`${baseUrl}/products`);
    setProducts(productList.data);
  }

  function removeFromCart(cartItemId: string) {
    delete cart[cartItemId];
    localStorage.setItem('cart', JSON.stringify(cart));
    setCart(JSON.parse(JSON.stringify(cart)));
  }

  function addToCart(cartItem: item) {
    if (cart[cartItem.id]) {
      cart[cartItem.id].amount += cartItem.amount;
    } else {
      cart[cartItem.id] = cartItem;
    }
    if (cart[cartItem.id].amount > cart[cartItem.id].product.stock) {
      cart[cartItem.id].amount = cart[cartItem.id].product.stock;
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    setCart(JSON.parse(JSON.stringify(cart)));
  }

  function clearCart() {
    localStorage.removeItem('cart');
    setCart({});
  }

  function checkout() {
    // const productList = products.map((p) => {
    //   if (cart[p.name]) {
    //     p.stock = p.stock - cart[p.name].amount;
    //     axios.put(`${baseUrl}/products/${p.id}`, { ...p });
    //   }
    //   return p;
    // });
    // setProducts(productList);
    // Process card
    // If successful
    // clearCart();
  }

  return (
    <Router>
      <NavBar user={user} cart={cart} logout={logout} />
      <Switch>
        <Route
          exact
          path="/"
          component={() => (
            <ProductList products={products} addToCart={addToCart} />
          )}
        />
        <Route
          exact
          path="/login"
          component={() => <Login user={user} handleLogin={login} />}
        />
        <Route
          exact
          path="/cart"
          component={() => (
            <Cart
              cart={cart}
              removeFromCart={removeFromCart}
              clearCart={clearCart}
              checkout={checkout}
              user={user}
            />
          )}
        />
        <Route
          exact
          path="/add-product"
          component={() => <AddProduct user={user} />}
        />
        <Route
          exact
          path="/products"
          component={() => (
            <ProductList products={products} addToCart={addToCart} />
          )}
        />
        <Route
          exact
          path="/signup"
          component={() => <Signup user={user} handleSignup={signUp} />}
        />
        <Route
          exact
          path="/checkoutContact"
          component={() => <CheckoutContact />}
        />
        <Route
          exact
          path="/checkoutShipping"
          component={() => <CheckoutShipping />}
        />
        <Route
          exact
          path="/checkoutPayment"
          component={() => <CheckoutPayment />}
        />
      </Switch>
    </Router>
  );
}
